package main;

import net.rim.device.api.gps.BlackBerryLocation;
import net.rim.device.api.ui.UiApplication;
import rimx.location.simplelocation.SimpleLocationListener;
import rimx.location.simplelocation.SimpleLocationProvider;

public class SimpleLocation extends UiApplication implements
		SimpleLocationListener {

	private int fixCount = 0, gpsFixCount = 0, geoFixCount = 0;

	private static SimpleLocationProvider simpleProvider;
	private static BlackBerryLocation location;

	private static void GetSingleLocationPosition() {
		try {

			simpleProvider = new SimpleLocationProvider();
			location = simpleProvider.getLocation(30);

			if (location != null && location.isValid()) {
				System.out.println("Latitude "
						+ location.getQualifiedCoordinates().getLatitude());
				System.out.println("Longitude "
						+ location.getQualifiedCoordinates().getLongitude());
				System.out.println("Horizontal Accuracy "
						+ location.getQualifiedCoordinates()
								.getHorizontalAccuracy());
				System.out.println("Obtained single location.");
			} else {
				System.out.println("Failed to obtain location!");
			}
		} catch (Exception e) {
			System.out.println("Exception on GetSingleLocationPosition "
					+ e.getMessage());
		}

	}
	
	public void locationEvent(int event, Object eventData) {
		synchronized (UiApplication.getEventLock()) {
			if (event == SimpleLocationListener.EVENT_GPS_LOCATION) {
				location = (BlackBerryLocation) eventData;
				System.out.println(""
						+ location.getQualifiedCoordinates().getLatitude()
						+ ", "
						+ location.getQualifiedCoordinates().getLongitude());
				System.out.println("GPS");
				fixCount++;
				gpsFixCount++;
				System.out.println(Integer.toString(fixCount));
				System.out.println(Integer.toString(gpsFixCount));
			} else if (event == SimpleLocationListener.EVENT_CELL_GEOLOCATION) {
				location = (BlackBerryLocation) eventData;
				System.out.println(""
						+ location.getQualifiedCoordinates().getLatitude()
						+ ", "
						+ location.getQualifiedCoordinates().getLongitude());
				System.out.println("Cell Tower Geolocation");
				fixCount++;
				geoFixCount++;
				System.out.println(Integer.toString(fixCount));
				System.out.println(Integer.toString(geoFixCount));
			} else if (event == SimpleLocationListener.EVENT_WLAN_GEOLOCATION) {
				location = (BlackBerryLocation) eventData;
				System.out.println(" "
						+ location.getQualifiedCoordinates().getLatitude()
						+ ", "
						+ location.getQualifiedCoordinates().getLongitude());
				System.out.println("WLAN Geolocation");
				fixCount++;
				geoFixCount++;
				System.out.println(Integer.toString(fixCount));
				System.out.println(Integer.toString(geoFixCount));
			} else if (event == SimpleLocationListener.EVENT_UNKNOWN_MODE) {
				location = (BlackBerryLocation) eventData;
				System.out.println(""
						+ location.getQualifiedCoordinates().getLatitude()
						+ ", "
						+ location.getQualifiedCoordinates().getLongitude());
				System.out.println("Unknown");
				fixCount++;
				System.out.println(Integer.toString(fixCount));
			} else if (event == SimpleLocationListener.EVENT_ACQUIRING_LOCATION) {
				System.out.println("EVENT_ACQUIRING_LOCATION - attempt = "
						+ eventData);
			} else if (event == SimpleLocationListener.EVENT_LOCATION_FAILED) {
				System.out.println("EVENT_LOCATION_FAILED - attempt = "
						+ eventData);
			}
		}
	}

	public void debugLog(String msg) {
		synchronized (UiApplication.getEventLock()) {
			System.out.println(msg);
		}
	}
	
	public static void main(String[] args){
		SimpleLocation app = new SimpleLocation();		
		app.enterEventDispatcher();
		GetSingleLocationPosition();
	} 

}
